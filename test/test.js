#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict'

require('chromedriver')

const execSync = require('child_process').execSync,
  expect = require('expect.js'),
  path = require('path')
const assert = require('assert')

const By = require('selenium-webdriver').By,
  until = require('selenium-webdriver').until,
  Key = require('selenium-webdriver').Key,
  Builder = require('selenium-webdriver').Builder

const username = process.env.TEST_USERNAME || process.env.USERNAME
const password = process.env.TEST_PASSWORD || process.env.PASSWORD

if (!username || !password) {
  console.log('USERNAME and PASSWORD env vars need to be set')
  process.exit(1)
}

describe('Application life cycle test', function () {
  this.timeout(0)

  let server
  const browser = new Builder().forBrowser('chrome').build()

  const LOCATION = 'test'
  const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' }
  const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000

  let app

  const getAppInfo = () => {
    const inspect = JSON.parse(execSync('cloudron inspect'))
    app = inspect.apps.filter(a => a.location === LOCATION || a.location === LOCATION + '2')[0]
    expect(app).to.be.an('object')
  }

  const login = () => browser.get(`https://${app.fqdn}/login`)
    .then(() => browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT))
    .then(() => browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username))
    .then(() => browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password))
    .then(() => browser.findElement(By.tagName('form')).submit())
    .then(() => browser.wait(until.elementLocated(By.css(`body[data-api^="https://${app.fqdn}/"]`)), TIMEOUT))

  const isWizardDisplayed = () => browser.get(`https://${app.fqdn}/`)
    .then(() => browser.wait(until.elementLocated(By.css(`body[data-api^="https://${app.fqdn}/"]`)), TIMEOUT))
    .then(() => browser.findElement(By.className('wizard')).isDisplayed())
    .then(wizardDisplayed => assert(wizardDisplayed))

  const finishWizard = () => browser.get(`https://${app.fqdn}/`)
    .then(() => browser.wait(until.elementLocated(By.css(`body[data-api^="https://${app.fqdn}/"]`)), TIMEOUT))
    .then(() => browser.sleep(500))
    .then(() => browser.findElement(By.css('.wizard .wgroup_finish .button')).click())

  const isMainPageDisplayed = () => browser.get(`https://${app.fqdn}/`)
    .then(() => browser.wait(until.elementLocated(By.css(`body[data-api^="https://${app.fqdn}/"]`)), TIMEOUT))
    .then(() => browser.findElement(By.css('html body div.block.header div.navigation a.logo')).isDisplayed())
    .then(logoDisplayed => assert(logoDisplayed))

  const logout = () => browser.get(`https://${app.fqdn}/logout`)
    .then(() => browser.sleep(2000)) // wait for "connection lost" error
    .then(() => browser.get(`https://${app.fqdn}/login`))
    .then(() => browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT))

  before(() => {
    if (!password) throw new Error('PASSWORD env var not set')

    const seleniumJar = require('selenium-server-standalone-jar')
    const SeleniumServer = require('selenium-webdriver/remote').SeleniumServer
    server = new SeleniumServer(seleniumJar.path, { port: 4444 })
    server.start()
  })

  after(() => {
    browser.quit()
    server.stop()
  })

  xit('build app', () => { execSync('cloudron build', EXEC_ARGS) })

  it('install app', () => { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS) })

  it('can get app information', getAppInfo)

  it('can login', login)
  it('is wizard displayed', isWizardDisplayed)
  it('finish wizard', finishWizard)
  it('is main page displayed', isMainPageDisplayed)
  it('can logout', logout)

  it('can restart app', () => { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS) })
  it('can login', login)
  it('is main page displayed', isMainPageDisplayed)
  it('can logout', logout)

  it('backup app', () => { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS) })
  it('restore app', () => {
    const backups = JSON.parse(execSync('cloudron backup list --raw'))
    execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS)
    execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS)
    const inspect = JSON.parse(execSync('cloudron inspect'))
    app = inspect.apps.filter(a => a.location === LOCATION)[0]
    execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS)
  })

  it('can login', login)
  it('is main page displayed', isMainPageDisplayed)
  it('can logout', logout)

  it('move to different location', () => { execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS) })
  it('can get app information', getAppInfo)
  it('can login', login)
  it('is main page displayed', isMainPageDisplayed)
  it('can logout', logout)

  it('uninstall app', () => { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS) })

  // test update
  it('can install app', () => { execSync(`cloudron install --appstore-id io.cloudron.couchpotato --location ${LOCATION}`, EXEC_ARGS) })
  it('can get app information', getAppInfo)
  it('can update', () => { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS) })
  it('can login', login)
  it('is main page displayed', isMainPageDisplayed)
  it('can logout', logout)
  it('uninstall app', () => { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS) })
})
