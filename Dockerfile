FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4
MAINTAINER Mehdi Kouhen <arantes555@gmail.com>

ARG COUCHPOTATO_VERSION="7260c12f72447ddb6f062367c6dfbda03ecd4e9c"

RUN apt-get update -y && \
    apt-get install -y locales && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# Locales so apps are happy
RUN locale-gen en_US.UTF-8
ENV LANG="en_US.UTF-8"
ENV LANGUAGE="en_US:en"
ENV LC_ALL="en_US.UTF-8"

## CouchPotato
RUN mkdir -p /app/code/couchpotato
RUN pip install --upgrade pyopenssl lxml
RUN curl -L "https://github.com/CouchPotato/CouchPotatoServer/archive/${COUCHPOTATO_VERSION}.tar.gz" \
    | tar -xz --strip-components=1 -C /app/code/couchpotato

COPY start.sh /app/code/

EXPOSE 5050

CMD [ "/app/code/start.sh" ]
