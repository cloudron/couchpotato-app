# CouchPotato

Automatic Video Library Manager for Movies.

## Building

### Cloudron
The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
git clone https://git.cloudron.io/mehdi/couchpotato-app.git
cd couchpotato-app
cloudron build
cloudron install
```
