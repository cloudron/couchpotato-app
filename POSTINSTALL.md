On first start, CouchPotato will display the wizard screen. Please note the following:

* Do *not* change the port on which CouchPotato is listening.

* Adding a username a password would be redundant, as CouchPotato is already protected by Cloudron authentication.

You may edit the other settings to your liking.
